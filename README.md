# AllCAPS

A silly Chrome extension designed to make your entire browser experience in CAPITAL LETTERS. [Download it from the Chrome App Store](https://chrome.google.com/webstore/detail/all-caps/olidncljahhkpbglpchphhnipcckeihg?utm_source=plus).

Contributions are accepted with gratitude. Feel free to take a look at issues marked with "enhancement" or "todo" to see what we need :)